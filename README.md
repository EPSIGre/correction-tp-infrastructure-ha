# Infrastructure haute disponibilité

## Contexte

On a 2 machines Debian9 avec chacune une IP et un nom DNS
- e.cours-webservice.arrobe.fr IN A 178.32.58.199
- f.cours-webservice.arrobe.fr IN A 178.32.138.193

On a également une entrée DNS de type A qui pointe sur les 2 IPs
- grp3.cours-webservice.arrobe.fr IN A 178.32.58.199
- grp3.cours-webservice.arrobe.fr IN A 178.32.138.193

Le but est d'installer un site web sur les 2 machines et de les synchroniser (base de donnée et fichiers)
pour atteindre 99,99 % de disponibilité

Le site web de démo est un Wiki : [BookStack](https://www.bookstackapp.com/)

## Etape 0 : Sécutité

### Sur les 2 machines :

```sh
# Changer le mot de passe par défaut
passwd

# Serveur à l'heure avec NTP
apt install ntp
systemctl enable ntp
nano /etc/ntp.conf
```

On remplace les serveurs :

```sh
pool 0.debian.pool.ntp.org iburst
pool 1.debian.pool.ntp.org iburst
pool 2.debian.pool.ntp.org iburst
pool 3.debian.pool.ntp.org iburst
```

par

```sh
pool 0.fr.pool.ntp.org iburst
pool 1.fr.pool.ntp.org iburst
pool 2.fr.pool.ntp.org iburst
pool 3.fr.pool.ntp.org iburst
```

```sh
# On relance le client NTP pour prendre en compte les nouveaux serveurs :
systemctl restart ntp

# On test
ntpq -p
date
```

```sh
# Installer les mises a jour de sécurité automatiquement
apt install cron-apt
grep security /etc/apt/sources.list > /etc/apt/sources.list.d/security.list
nano /etc/apt/sources.list
```

On commente la ligne de sécurité (on a déplacé la ligne dans un fichier dédié) :

```sh
deb http://debian.mirrors.ovh.net/debian stretch main
#deb http://security.debian.org/debian-security stretch/updates main
```

```sh
# On vide le cache local d'APT
apt clean
# On le recharge pour vérifier si on a pas d'erreur de syntaxe dans nos sources.list
apt update
# On configure cron-apt pour ne prendre que les patchs de sécurité
nano /etc/cron-apt/config
```

Le fichier est comme ça :

```sh
# Configuration for cron-apt. For further information about the possible
# configuration settings see /usr/share/doc/cron-apt/README.gz.
APTCOMMAND=/usr/bin/apt
OPTIONS="-o quiet=1 -o Dir::Etc::SourceList=/etc/apt/sources.list.d/security.list"
```

```sh
# cron-apt a été prévu pour télécharger les paquets à mettre à jour pour ceux qui ont encore des modem 56K
# On veut pas télécharger et installer les patchs de sécurité donc on doit modifier ça :
nano /etc/cron-apt/action.d/3-download
```

On obtient un fichier comme celui-là :

```sh
autoclean -y
upgrade -y -o APT::Get::Show-Upgraded=true
```

```sh
# On test maintenant ça à la "main" :
cron-apt
# Si ça dure un peu, c'est normal, on a commencé le TP il y a quelques semaines et entre-temps, il y a eu des patchs à appliquer...
# Il reste à vérifier et adapter si besoin les horaires d'exécution plannifiée :
cat /etc/cron.d/cron-apt
# Par défaut, tous les jours à 4h du matin
```

## Etape 1 : Conf. Debian

On commence par une dificultée : Debian 9 est en PHP  7.0 et on aura besoin de PHP 7.1 pour faire du Symfony 4
... et tant qu'a customiser, on va prendre la 7.2

A faire sur les 2 machines :

```sh
# PHP, Apache, MariaDB, Certbot & autres trucs de base
apt update
apt install lsb-release
wget https://packages.sury.org/php/README.txt
bash README.txt
apt install php7.2 php7.2-mysql php7.2-xml php7.2-zip php7.2-curl php7.2-memcached php7.2-tidy php7.2-mbstring php7.2-gd apache2 mariadb-server git vim curl zip unzip htop iotop iftop certbot python-certbot-apache python-pip memcached

# Le depot de Sury instale PHP 7.2 ET PHP 7.3 ...
# Il faut juste "activer" le 7.2 à la place du 7.3
rm /etc/alternatives/php
ln -s /usr/bin/php7.2 /etc/alternatives/php

# Composer
wget https://getcomposer.org/installer
php installer --install-dir=/usr/local/bin/ --filename=composer
```

## Etape 2 : Install GlusterFS

### Sur machine 1 :

```sh
apt install glusterfs-server
systemctl enable glusterfs-server.service
mkdir -p /srv/TPHA

# Adapter avec le nom de la machine
gluster peer probe f.cours-webservice.arrobe.fr
openssl genrsa -out /etc/ssl/glusterfs.key 2048

# Adapter avec le nom de la machine
openssl req -new -x509 -key /etc/ssl/glusterfs.key -subj "/CN=e.cours-webservice.arrobe.fr" -out /etc/ssl/glusterfs.pem -days 3650
```

### Sur machine 2 :

```sh
apt install glusterfs-server
systemctl enable glusterfs-server.service
mkdir -p /srv/TPHA

# Adapter avec le nom de la machine
openssl genrsa -out /etc/ssl/glusterfs.key 2048

# Adapter avec le nom de la machine
openssl req -new -x509 -key /etc/ssl/glusterfs.key -subj "/CN=f.cours-webservice.arrobe.fr" -out /etc/ssl/glusterfs.pem -days 3650

# On copie le certificat le l'autre machine
scp e.cours-webservice.arrobe.fr:/etc/ssl/glusterfs.pem ~/.
# On agrège les 2 certificats
cat /etc/ssl/glusterfs.pem > /etc/ssl/glusterfs.ca
cat /root/glusterfs.pem >> /etc/ssl/glusterfs.ca
# On copie le resultat sur la 1er machine
scp /etc/ssl/glusterfs.ca e.cours-webservice.arrobe.fr:/etc/ssl/.

# Activer le chiffrement sur Gluster
touch /var/lib/glusterd/secure-access
systemctl restart glusterfs-server.service
```

### Sur machine 1 :

```sh
touch /var/lib/glusterd/secure-access
systemctl restart glusterfs-server.service

# On créé le volume Gluster
gluster volume create TPHA replica 2 transport tcp e.cours-webservice.arrobe.fr:/srv/TPHA f.cours-webservice.arrobe.fr:/srv/TPHA force

gluster volume set TPHA auth.ssl-allow '*'
gluster volume set TPHA client.ssl on
gluster volume set TPHA server.ssl on
gluster volume set TPHA network.ping-timeout "1"
gluster volume set TPHA cluster.favorite-child-policy 'mtime'
```

### Sur machine 2 :

```sh
gluster volume set TPHA auth.ssl-allow '*'
gluster volume set TPHA client.ssl on
gluster volume set TPHA server.ssl on
gluster volume set TPHA network.ping-timeout "1"

# ... et on démarre
gluster volume start TPHA

# Ok, la partie serveur est installée
# Pour tester (on peut le faire des 2 cotés)
gluster volume status

# On passe à la partie client
# On crée le point de montage
mkdir -p /mnt/gluster
mount -t glusterfs f.cours-webservice.arrobe.fr:/TPHA /mnt/gluster
```

### Sur machine 1 :

```sh
mkdir -p /mnt/gluster
mount -t glusterfs e.cours-webservice.arrobe.fr:/TPHA /mnt/gluster
```

Tout les dossiers sous forme de lien symboliques vers le montage Gluster sont automatiquement répliqués.

On va commencer par appliquer ça aux certificats de certbot.

### Sur machine 1 :

```sh
mv -f /etc/letsencrypt /mnt/gluster/
ln -s /mnt/gluster/letsencrypt /etc/
```

### Sur machine 2 :

```sh
rm -rf /etc/letsencrypt
ln -s /mnt/gluster/letsencrypt /etc/
```

Oui, maintenant, les certificats sont valables pour les 2 machines, automatiquement !

## Etape 3 : Install Galera

### Sur les 2 machines :

```sh
wget https://repo.percona.com/apt/percona-release_0.1-4.$(lsb_release -sc)_all.deb
dpkg -i percona-release_0.1-4.$(lsb_release -sc)_all.deb
apt update
apt install percona-xtrabackup socat

# On continue sur MariaDB
mysql -u root
```

```sql
CREATE USER 'galera'@'localhost' IDENTIFIED BY 'TPI4EPSIGre';
GRANT RELOAD, LOCK TABLES, REPLICATION CLIENT ON *.* TO 'galera'@'localhost';
FLUSH PRIVILEGES;
exit;
```

On utilise XTraBackup plutot que RSync
[Voir doc Galera](http://galeracluster.com/documentation-webpages/sst.html)
Par contre, on doit chiffrer nous-même les échanges

### Sur machine 1 :

```sh
certbot --apache certonly -d e.cours-webservice.arrobe.fr
nano /etc/mysql/conf.d/galera.cnf
```

On doit obtenir :

```
[mysql]
ssl-ca = /etc/letsencrypt/live/e.cours-webservice.arrobe.fr/chain.pem
ssl-key = /etc/letsencrypt/live/e.cours-webservice.arrobe.fr/privkey.pem
ssl-cert = /etc/letsencrypt/live/e.cours-webservice.arrobe.fr/cert.pem

[mysqld]
binlog_format = ROW
default_storage_engine = InnoDB
innodb_autoinc_lock_mode = 2
bind-address = 0.0.0.0
ssl-ca = /etc/letsencrypt/live/e.cours-webservice.arrobe.fr/chain.pem
ssl-key = /etc/letsencrypt/live/e.cours-webservice.arrobe.fr/privkey.pem
ssl-cert = /etc/letsencrypt/live/e.cours-webservice.arrobe.fr/cert.pem

wsrep_on = ON
wsrep_cluster_address = gcomm://e.cours-webservice.arrobe.fr,f.cours-webservice.arrobe.fr
wsrep_cluster_name = TPHA
wsrep_node_address = e.cours-webservice.arrobe.fr
wsrep_node_name = e
wsrep_provider = /usr/lib/galera/libgalera_smm.so
wsrep_provider_options = "gcache.size=512M;socket.checksum=2;socket.ssl_cipher=AES256-SHA"
wsrep_slave_threads = 4
wsrep_sst_auth=galera:TPI4EPSIGre
```

### Sur machine 2 :

```sh
service mysql stop
certbot --apache certonly -d f.cours-webservice.arrobe.fr
nano /etc/mysql/conf.d/galera.cnf
```

Vous avez le niveau pour adapter le fichier ...

### Sur machine 1 :

```sh
service mysql stop
# Lancer le cluster
galera_new_cluster
service mysql start --wsrep-new-cluster
```

### Sur machine 2 :

```sh
service mysql start
mysql -u root -e 'SELECT VARIABLE_VALUE as "cluster size" FROM INFORMATION_SCHEMA.GLOBAL_STATUS WHERE VARIABLE_NAME="wsrep_cluster_size"'
# Doit retourner "cluster size" = 2
```

### Sur machine 1 :

```sh
service mysql restart
```

## Etape 4 : Install BookStack

**Attention : autant on installe le code sur les 2 machines, autant on installe la base que sur une => Elle est répliqué par Galera**

### Sur les 2 machines :

... on suit la [doc officielle](https://www.bookstackapp.com/docs/admin/installation/)

```sh
cd /var/www
git clone https://github.com/BookStackApp/BookStack.git --branch release --single-branch
cd BookStack
composer install
cp .env.example .env
```

### Sur machine 1 :

```sh
# On crée un user MariaDB pour le site web
mysql -u root
```

```sql
CREATE USER wiki@localhost IDENTIFIED BY 'wiki';
CREATE DATABASE wiki;
GRANT ALL PRIVILEGES ON *.* TO 'wiki'@'localhost';
FLUSH PRIVILEGES;
exit;
```

### Sur les 2 machines :

Editer le fichier de conf et mettre les bonnes valeur pour base/user/password que vous venez de créer dans MariaDB

```sh
nano .env
```
### Sur machine 1 :

On partage via Gluster les dossiers qui contiennent des fichiers à synchroniser

```sh
mkdir /mnt/gluster/bookstack
mkdir /mnt/gluster/bookstack/public
mv /var/www/BookStack/public/uploads /mnt/gluster/bookstack/public/uploads
ln -s /mnt/gluster/bookstack/public/uploads /var/www/BookStack/public/
mv /var/www/BookStack/storage /mnt/gluster/bookstack/storage
ln -s /mnt/gluster/bookstack/storage /var/www/BookStack/
chown -R www-data:www-data /mnt/gluster/bookstack/
chown -R www-data:www-data /var/www/BookStack/bootstrap/cache
```

### Sur machine 2 :

```sh
rm -rf /var/www/BookStack/public/uploads
ln -s /mnt/gluster/bookstack/public/uploads /var/www/BookStack/public/
rm -rf /var/www/BookStack/storage
ln -s /mnt/gluster/bookstack/storage /var/www/BookStack/
chown -R www-data:www-data /var/www/BookStack/bootstrap/cache
```

### Sur machine 1 :

```sh
php artisan key:generate --force
php artisan migrate --force
```

### Sur machine 2 :

Copier coller la clé généré par 'php artisan' sur la machine 1, et la mettre dans le .env

### Sur les 2 machines :

```sh
# Configuration Apache
cd /etc/apache2/
a2enmod rewrite
systemctl restart apache2
cp sites-available/000-default.conf sites-available/grp3.cours-webservice.arrobe.fr.conf
nano sites-available/grp3.cours-webservice.arrobe.fr.conf
```

Editer le fichier de conf. Apache pour arriver à ça :

```apache
<VirtualHost *:80>
    ServerName grp3.cours-webservice.arrobe.fr
    ServerAdmin webmaster@localhost
    DocumentRoot /var/www/BookStack/public

    <Directory /var/www/BookStack/public>
        AllowOverride All
        Require all granted
    </Directory>

    ErrorLog ${APACHE_LOG_DIR}/error.log
    CustomLog ${APACHE_LOG_DIR}/access.log combined
</VirtualHost>
```

Activer le site :

```sh
a2ensite grp3.cours-webservice.arrobe.fr
systemctl reload apache2
```

[Tester le site](http://grp3.cours-webservice.arrobe.fr) u: admin@admin.com / pw : password

**et changer le mot de passe admin**

## Etape 5 : Passer le site en https

### Sur machine 1 :

```sh
certbot --apache certonly -d grp3.cours-webservice.arrobe.fr
```

... et répondez aux questions !

### Sur les 2 machines :

On change la conf Apache
```sh
a2enmod ssl
nano /etc/apache2/sites-enabled/grp3.cours-webservice.arrobe.fr.conf
```

On doit obtenir ça :

```apache
<VirtualHost *:80>
        ServerName grp3.cours-webservice.arrobe.fr
        ServerAdmin webmaster@localhost

        RewriteEngine On
        RewriteRule .* https://%{HTTP_HOST}%{REQUEST_URI} [R=301,L]
</VirtualHost>
<VirtualHost *:443>
    ServerName grp3.cours-webservice.arrobe.fr
    ServerAdmin webmaster@localhost
    DocumentRoot /var/www/BookStack/public

    <Directory /var/www/BookStack/public>
        AllowOverride All
        Require all granted
    </Directory>

    SSLEngine On
        SSLCertificateFile /etc/letsencrypt/live/grp3.cours-webservice.arrobe.fr/fullchain.pem
        SSLCertificateKeyFile /etc/letsencrypt/live/grp3.cours-webservice.arrobe.fr/privkey.pem

    ErrorLog ${APACHE_LOG_DIR}/error.log
    CustomLog ${APACHE_LOG_DIR}/access.log combined
</VirtualHost>
```

On recharge Apache et on test

```sh
systemctl restart apache2
```

On test [le site en http](http://grp3.cours-webservice.arrobe.fr) pour verifier la redirection

# Etape 6 Un site en A+

Tester le site [chez Qualys](https://www.ssllabs.com/ssltest)

### Sur les 2 machines :

```sh
a2enmod headers
nano /etc/apache2/conf-available/tls.conf
```

```apache
SSLCipherSuite EECDH+AESGCM:EDH+AESGCM:AES256+EECDH:AES256+EDH
SSLProtocol All -SSLv2 -SSLv3 -TLSv1 -TLSv1.1
SSLHonorCipherOrder On
Header setifempty Strict-Transport-Security "max-age=63072000; includeSubDomains; preload"
Header setifempty X-Frame-Options DENY
Header setifempty X-Content-Type-Options nosniff
SSLCompression Off
SSLUseStapling On
SSLStaplingCache "shmcb:logs/stapling-cache(86400)"
SSLSessionTickets Off
```

```sh
a2enconf tls
service apache2 restart
```

... et re-test [chez Qualys](https://www.ssllabs.com/ssltest)

# Etape 7 Load Balancer

### Sur les 2 machines :

```sh
a2enmod proxy_http
a2enmod proxy_balancer
a2enmod lbmethod_byrequests
systemctl restart apache2
nano /etc/apache2/ports.conf
```

Ajouter une ligne `Listen 666` (ou n'importe quel port de libre) juste après la ligne `Listen 80`

```sh
nano /etc/apache2/conf-available/balancer.conf
```

Copier et adapter ça :

```apache
SSLProxyEngine On
SSLProxyVerify none
SSLProxyCheckPeerCN Off
SSLProxyCheckPeerName Off
SSLProxyMachineCertificateChainFile /etc/letsencrypt/live/grp3.cours-webservice.arrobe.fr/fullchain.pem
SSLProxyMachineCertificateFile /etc/letsencrypt/live/grp3.cours-webservice.arrobe.fr/privkey.pem

<Proxy "balancer://TPHA">
    BalancerMember "https://e.cours-webservice.arrobe.fr:666"
    BalancerMember "https://f.cours-webservice.arrobe.fr:666"
</Proxy>
```

```sh
a2enconf balancer
nano /etc/apache2/sites-available/grp3.cours-webservice.arrobe.fr.conf
```

Copier et adapter ça :

```apache
<VirtualHost *:80>
        ServerName grp3.cours-webservice.arrobe.fr
        ServerAdmin webmaster@localhost

        RewriteEngine On
        RewriteRule .* https://%{HTTP_HOST}%{REQUEST_URI} [R=301,L]
</VirtualHost>
<VirtualHost *:443>
    ServerName grp3.cours-webservice.arrobe.fr
    ServerAdmin webmaster@localhost

    ProxyPass        "/" "balancer://TPHA/"
    ProxyPassReverse "/" "balancer://TPHA/"
    ProxyPreserveHost On

    SSLEngine On
    SSLCertificateFile /etc/letsencrypt/live/grp3.cours-webservice.arrobe.fr/fullchain.pem
    SSLCertificateKeyFile /etc/letsencrypt/live/grp3.cours-webservice.arrobe.fr/privkey.pem
</VirtualHost>
<VirtualHost *:666>
    ServerName grp3.cours-webservice.arrobe.fr
    ServerAdmin webmaster@localhost
    DocumentRoot /var/www/BookStack/public

    <Directory /var/www/BookStack/public>
        AllowOverride All
        Require all granted
    </Directory>

    SSLEngine On
    SSLCertificateFile /etc/letsencrypt/live/grp3.cours-webservice.arrobe.fr/fullchain.pem
    SSLCertificateKeyFile /etc/letsencrypt/live/grp3.cours-webservice.arrobe.fr/privkey.pem

    ErrorLog ${APACHE_LOG_DIR}/error.log
    CustomLog ${APACHE_LOG_DIR}/access.log combined
</VirtualHost>
```

```sh
systemctl restart apache2
```

Tester avec le navigateur
